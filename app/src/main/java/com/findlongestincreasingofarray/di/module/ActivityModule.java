package com.findlongestincreasingofarray.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.findlongestincreasingofarray.Utils.rx.AppSchedulerProvider;
import com.findlongestincreasingofarray.Utils.rx.SchedulerProvider;
import com.findlongestincreasingofarray.di.ActivityContext;
import com.findlongestincreasingofarray.di.PerActivity;
import com.findlongestincreasingofarray.ui.main.MainInteractor;
import com.findlongestincreasingofarray.ui.main.MainMvpInteractor;
import com.findlongestincreasingofarray.ui.main.MainMvpPresenter;
import com.findlongestincreasingofarray.ui.main.MainMvpView;
import com.findlongestincreasingofarray.ui.main.MainPresenter;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView, MainMvpInteractor> provideMainMvpPresenter(
            MainPresenter<MainMvpView, MainMvpInteractor> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpInteractor provideMainMvpInteractor(MainInteractor interactor) {
        return interactor;
    }
}
