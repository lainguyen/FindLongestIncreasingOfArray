package com.findlongestincreasingofarray.di.module;

import android.app.Application;
import android.content.Context;

import com.findlongestincreasingofarray.Utils.AppConstants;
import com.findlongestincreasingofarray.data.prefs.AppPreferencesHelper;
import com.findlongestincreasingofarray.data.prefs.PreferencesHelper;
import com.findlongestincreasingofarray.di.ApplicationContext;
import com.findlongestincreasingofarray.di.PreferenceInfo;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return AppConstants.PREF_NAME;
    }

    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }
}
