package com.findlongestincreasingofarray.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface PerActivity {
}
