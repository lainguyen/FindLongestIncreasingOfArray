package com.findlongestincreasingofarray.di.component;

import android.app.Application;
import android.content.Context;

import com.findlongestincreasingofarray.DemoApplication;
import com.findlongestincreasingofarray.data.prefs.PreferencesHelper;
import com.findlongestincreasingofarray.di.ApplicationContext;
import com.findlongestincreasingofarray.di.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(DemoApplication application);

    @ApplicationContext
    Context context();

    Application application();

    PreferencesHelper preferencesHelper();
}
