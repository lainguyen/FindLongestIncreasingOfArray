package com.findlongestincreasingofarray.di.component;

import com.findlongestincreasingofarray.di.PerActivity;
import com.findlongestincreasingofarray.di.module.ActivityModule;
import com.findlongestincreasingofarray.ui.main.MainActivity;

import dagger.Component;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

@PerActivity
@Component(modules = ActivityModule.class, dependencies = ApplicationComponent.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

}
