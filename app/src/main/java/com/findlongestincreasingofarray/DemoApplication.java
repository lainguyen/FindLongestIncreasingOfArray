package com.findlongestincreasingofarray;

import android.app.Application;

import com.findlongestincreasingofarray.di.component.ApplicationComponent;
import com.findlongestincreasingofarray.di.component.DaggerApplicationComponent;
import com.findlongestincreasingofarray.di.module.ApplicationModule;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public class DemoApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.inject(this);

    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
