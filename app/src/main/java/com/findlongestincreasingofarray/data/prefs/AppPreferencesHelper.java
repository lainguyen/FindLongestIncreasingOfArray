package com.findlongestincreasingofarray.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.findlongestincreasingofarray.di.ApplicationContext;
import com.findlongestincreasingofarray.di.PreferenceInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context,
                                @PreferenceInfo String prefFileName) {
        mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

}
