package com.findlongestincreasingofarray.Utils;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public class AppConstants {

    public static final String PREF_NAME = "demo_pref";

    public static final String TIMESTAMP_FORMAT = "yyyyMMdd_HHmmss";

    // extras keys
    public static final String KEY_BEGIN_INDEX = "begin_index";
    public static final String KEY_END_INDEX = "end_index";
}
