package com.findlongestincreasingofarray.Utils.rx;

import io.reactivex.Scheduler;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public interface SchedulerProvider {

    Scheduler ui();

    Scheduler computation();

    Scheduler io();

}
