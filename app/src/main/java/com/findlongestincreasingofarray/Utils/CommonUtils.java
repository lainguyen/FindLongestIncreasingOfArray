package com.findlongestincreasingofarray.Utils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.text.TextUtils;

import com.findlongestincreasingofarray.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public final class CommonUtils {

    private static final String TAG = "CommonUtils";

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String getTimeStamp() {
        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT, Locale.US).format(new Date());
    }

    /**
     * used to check numbers array is valid or not, if numbers is null
     * or content any non number char
     *
     * example valid array: "1, 9, 3, 2, 2, 4, 5, 7, 8, 9"
     * example invalid array: "1,2,a,v,2,3"
     *
     * @param numbersString
     *
     * @return true if valid, false if invalid
     * */
    public static boolean stringIsNumberArray(String numbersString) {
        if (TextUtils.isEmpty(numbersString)) {
            return false;
        }

        String[] numberArray = numbersString.split(",");
        boolean existingNonNumber = false;

        for (String number: numberArray) {
            if (!TextUtils.isDigitsOnly(number)) {
                existingNonNumber = true;
                break;
            }
        }

        return !existingNonNumber;

    }

    /**
     * this used to convert string to int array,
     * example "1,2,3,4,5" => [1,2,3,4,5]
     *
     * @param numbers int array in string
     *
     * @return int array if numbers is valid, null if numbers is invalid
     * */
    public static int[] convertStringToIntArray(String numbers) {
        if (!stringIsNumberArray(numbers)) {
            return null;
        }
        String[] numberStringArray = numbers.split(",");

        if (numbers.length() <= 0) {
            return null;
        }

        int[] result = new int[numberStringArray.length];
        try {
            for (int i = 0; i < numberStringArray.length; i++) {
                result[i] = Integer.valueOf(numberStringArray[i]);
            }

            return result;
        } catch (NumberFormatException e) {
            return null;
        }
    }
}