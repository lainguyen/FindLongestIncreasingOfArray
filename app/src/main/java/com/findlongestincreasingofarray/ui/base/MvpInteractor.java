package com.findlongestincreasingofarray.ui.base;

import com.findlongestincreasingofarray.data.prefs.PreferencesHelper;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public interface MvpInteractor {

    PreferencesHelper getPreferencesHelper();
}
