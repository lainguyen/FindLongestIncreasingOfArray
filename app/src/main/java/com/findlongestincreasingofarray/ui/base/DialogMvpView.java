package com.findlongestincreasingofarray.ui.base;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public interface DialogMvpView extends MvpView {

    void dismissDialog(String tag);
}