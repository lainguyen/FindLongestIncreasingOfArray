package com.findlongestincreasingofarray.ui.base;

import com.findlongestincreasingofarray.data.prefs.PreferencesHelper;

import javax.inject.Inject;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public class BaseInteractor implements MvpInteractor {

    private final PreferencesHelper mPreferencesHelper;

    @Inject
    public BaseInteractor(PreferencesHelper preferencesHelper) {
        mPreferencesHelper = preferencesHelper;
    }

    @Override
    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

}
