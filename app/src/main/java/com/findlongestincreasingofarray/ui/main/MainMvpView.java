package com.findlongestincreasingofarray.ui.main;

import com.findlongestincreasingofarray.ui.base.MvpView;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public interface MainMvpView extends MvpView {

    /**
     * This used to show beginIndex of the longest increasing of Array
     *
     * @param indexs array 2 items, index 0 is begin, index 1 is end of longest increasing of numberArray @
     *
     * */
    void showBeginEndIndicate(int[] indexs);

    /**
     * This used to show invalid message in case numbers array is null or empty or existing non number char
     * */
    void showMessageNumberArrayInvalid(int invalidMessageRes);

}
