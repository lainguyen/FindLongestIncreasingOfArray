package com.findlongestincreasingofarray.ui.main;

import com.findlongestincreasingofarray.R;
import com.findlongestincreasingofarray.Utils.CommonUtils;
import com.findlongestincreasingofarray.Utils.rx.SchedulerProvider;
import com.findlongestincreasingofarray.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public class MainPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends BasePresenter<V, I> implements MainMvpPresenter<V, I> {

    @Inject
    public MainPresenter(I mvpInteractor,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        super(mvpInteractor, schedulerProvider, compositeDisposable);
    }

    @Override
    public void findLongestIncreasingOfArray(String numbers) {

        getMvpView().hideKeyboard();
        getMvpView().showLoading();

        getCompositeDisposable()
                .add(getInteractor().findIndexLongestIncreasingOfArray(CommonUtils.convertStringToIntArray(numbers))
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<int[]>() {
                    @Override
                    public void accept(int[] index) throws Exception {
                        if (!isViewAttached()) {
                            return;
                        }

                        getMvpView().hideLoading();
                        if (index != null) {
                            getMvpView().showBeginEndIndicate(index);
                        } else {
                            getMvpView().onError(R.string.some_error);
                        }

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getMvpView().hideLoading();
                        getMvpView().onError(R.string.some_error);
                    }
                }));
    }

    @Override
    public boolean isNumberArray(String numbers) {
        if (!CommonUtils.stringIsNumberArray(numbers)) {
            getMvpView().showMessageNumberArrayInvalid(R.string.invalid_number_array);
            return false;
        }
        return true;
    }
}
