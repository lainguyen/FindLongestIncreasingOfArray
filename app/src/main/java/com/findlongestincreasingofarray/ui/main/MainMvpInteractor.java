package com.findlongestincreasingofarray.ui.main;

import com.findlongestincreasingofarray.ui.base.MvpInteractor;

import io.reactivex.Observable;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public interface MainMvpInteractor extends MvpInteractor {

    /**
     * This returns the beginning and ending indicates of the longest increasing of numberArray
     * For example:
     * A = {1, 9, 3, 2, 2, 4, 5, 7, 8, 9}
     * Result: 4, 9
     * @param numbers number array string, separated by ','
     *
     * @return array 2 items, index 0 is begin, index 1 is end of longest increasing of numberArray
     * */
    Observable<int[]> findIndexLongestIncreasingOfArray(int[] numbers);

}
