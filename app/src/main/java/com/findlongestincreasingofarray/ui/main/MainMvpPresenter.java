package com.findlongestincreasingofarray.ui.main;

import com.findlongestincreasingofarray.ui.base.MvpPresenter;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public interface MainMvpPresenter<V extends MainMvpView, I extends MainMvpInteractor>
        extends MvpPresenter<V, I> {

    /**
     * This returns the beginning and ending indicates of the longest increasing of numberArray
     * For example:
     * A = {1, 9, 3, 2, 2, 4, 5, 7, 8, 9}
     * Result: 4, 9
     * @param numbers number array string, separated by ','
     *
     * @return array 2 items, index 0 is begin, index 1 is end of longest increasing of numberArray
     * */
    void findLongestIncreasingOfArray(String numbers);

    /**
     * This used to check whether the numbers string is valid int array or not.
     *
     * @return true if numbers is int array, Otherwise false
     * */
    boolean isNumberArray(String numbers);

}
