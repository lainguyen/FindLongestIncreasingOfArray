package com.findlongestincreasingofarray.ui.main;

import com.findlongestincreasingofarray.data.prefs.PreferencesHelper;
import com.findlongestincreasingofarray.ui.base.BaseInteractor;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by lainguyen on 8/26/18.
 * Demo Project
 */

public class MainInteractor extends BaseInteractor implements MainMvpInteractor {

    @Inject
    public MainInteractor(PreferencesHelper preferencesHelper) {
        super(preferencesHelper);
    }

    @Override
    public Observable<int[]> findIndexLongestIncreasingOfArray(int[] numbers) {

        if (numbers == null) {
            return null;
        }

        int lastBeginIndex = 0; // used to save last begin index in looping
        int lastEndIndex = 0; // // used to save last end index in looping
        int lastAmount = 0; // used to save max amount index


        int beginIndex = lastBeginIndex; // used to save begin index in current looping
        int endIndex = lastEndIndex; // used to save begin index in current looping
        int amount = 1; // used to save total index in current loop


        for (int i = 0; i < numbers.length - 1; i++) {

            if (numbers[i] < numbers[i+1]) {
                // if before < after, save end index and increase amount
                endIndex = i+1;
                amount++;

                // in case we looped numbers.length - 1, if case last amount < current amount index, update state
                if (i == numbers.length - 2) {
                    lastBeginIndex = beginIndex;
                    lastEndIndex = endIndex;
                }
            } else {
                // if before > after, compare last amount and current amount, if last amount < current amount, change range index
                if (lastAmount < amount) {
                    lastBeginIndex = beginIndex;
                    lastEndIndex = endIndex;
                    lastAmount = amount;
                }

                // reset current state
                beginIndex = i+1;
                endIndex = i+1;
                amount = 1;
            }

        }

        return Observable.just(new int[]{ lastBeginIndex, lastEndIndex });
    }
}
