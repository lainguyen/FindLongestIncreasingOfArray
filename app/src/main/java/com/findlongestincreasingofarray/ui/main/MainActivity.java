package com.findlongestincreasingofarray.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import com.findlongestincreasingofarray.R;
import com.findlongestincreasingofarray.Utils.AppConstants;
import com.findlongestincreasingofarray.ui.base.BaseActivity;
import com.findlongestincreasingofarray.ui.second.SecondActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainMvpView {

    @BindView(R.id.etNumberArray)
    EditText etNumberArray;

    @Inject MainMvpPresenter<MainMvpView, MainMvpInteractor> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this));

        mPresenter.onAttach(this);

        setUp();

    }

    @Override
    protected void setUp() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        etNumberArray.setText("1,9,3,2,2,4,5,7,8,9");
    }

    @OnClick(R.id.btnFind)
    public void onButtonFindClick() {
        if (mPresenter.isNumberArray(etNumberArray.getText().toString())) {
            mPresenter.findLongestIncreasingOfArray(etNumberArray.getText().toString());
        }
    }

    @Override
    public void showBeginEndIndicate(int[] indexs) {

        // start second activity
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra(AppConstants.KEY_BEGIN_INDEX, indexs[0]);
        intent.putExtra(AppConstants.KEY_END_INDEX, indexs[1]);

        startActivity(intent);
    }

    @Override
    public void showMessageNumberArrayInvalid(int invalidMessageRes) {
        Toast.makeText(this, invalidMessageRes, Toast.LENGTH_LONG).show();
    }
}
