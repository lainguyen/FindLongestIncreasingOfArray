package com.findlongestincreasingofarray.ui.second;

import android.os.Bundle;
import android.widget.TextView;

import com.findlongestincreasingofarray.R;
import com.findlongestincreasingofarray.Utils.AppConstants;
import com.findlongestincreasingofarray.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondActivity extends BaseActivity {


    @BindView(R.id.tvBeginIndex)
    TextView tvBeginIndex;

    @BindView(R.id.tvEndIndex)
    TextView tvEndIndex;

    int beginIndex, endIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        setUnBinder(ButterKnife.bind(this));

        setUp();
    }

    @Override
    protected void setUp() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            if (extras.containsKey(AppConstants.KEY_BEGIN_INDEX) && extras.containsKey(AppConstants.KEY_END_INDEX)) {
                beginIndex = extras.getInt(AppConstants.KEY_BEGIN_INDEX);
                endIndex = extras.getInt(AppConstants.KEY_END_INDEX);

                showIndex();
            }
        }
    }

    private void showIndex() {

        tvBeginIndex.setText(String.valueOf(beginIndex));
        tvEndIndex.setText(String.valueOf(endIndex));
    }
}
